<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;

class CreateArticle extends Component
{
    use WithFileUploads;

    public $title;
    public $subtitle;
    public $body;
    public $img;
    public $category;




    public function post()
    {
        $category = Category::find($this->category);
        $article= $category->articles()->create([
            'title'=>$this->title,
            'subtitle'=>$this->subtitle,
            'body'=>$this->body,
            'img'=>$this->img->store('public/img')
        ]);
        $article->user()->associate(Auth::user());
        $article->save();
        return redirect(route('create'))->with('create','Article created correctly');
    }





    public function render()
    {
        return view('livewire.create-article');
    }
}
