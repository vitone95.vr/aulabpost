<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Mail\RequestRoleMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function home() {
        $articles = Article::orderBy("created_at", "desc")->take(3)->get();
        return view('welcome',compact("articles"));
    }

    public function create() {
        return view('createArticle');
    }

    public function detail(Article $article) {
        return view('detailArticle', compact('article'));
    }

    public function categorysearch(Category $category) {
        return view('categorySearch', compact('category'));
    }

    public function authorsearch(User $user) {
        return view('authorSearch', compact('user'));
    }

    public function workWithUs(){
        return view('workWithUs');
    }

    public function sendRoleRequest(Request $request){
        $user = Auth::user();
        $role = $request->input('role');
        $email = $request->input('email');
        $presentation = $request->input('presentation');
        $requestMail = new RequestRoleMail(compact('role','email','presentation'));
        Mail::to('admin@admin.it')->send($requestMail); 
        switch ($role){
            case 'admin':
                $user->is_admin= null;
                break;
            case 'revisor':
                $user->is_revisor= null;
                break;
            case 'writer':
                $user->is_writer= null;
                break;
        }
        $user->update();
        return redirect()->route('home')->with('role','Your request will be shortly revised, thanks for contacting us');
    }

    public function dashboard(){
        $adminRequests = User::where('is_admin', null)->get();
        $revisorRequests = User::where('is_revisor', null)->get();
        $writerRequests = User::where('is_writer', null)->get();
        return view('adminDashboard', compact('adminRequests','revisorRequests','writerRequests'));
    }

    public function makeAdmin(User $user){
        $user->is_admin= true;
        $user->save();
        return redirect()->route('adminDashboard')->with('admin','User is now Admin');
    }

    public function makeRevisor(User $user){
        $user->is_revisor= true;
        $user->save();
        return redirect()->route('adminDashboard')->with('revisor','User is now Revisor');
    }
    
    public function makeWriter(User $user){
        $user->is_writer= true;
        $user->save();
        return redirect()->route('adminDashboard')->with('writer','User is now Writer');
    }
}
