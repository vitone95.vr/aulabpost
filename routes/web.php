<?php

use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'] )->name('home');

Route::get('/create-article', [PublicController::class, 'create'] )->name('create');

Route::get('/detail-article/{article}', [PublicController::class, 'detail'] )->name('detail');

Route::get('/category/{category}', [PublicController::class, 'categorysearch'] )->name('categorysearch');

Route::get('/author/{user}', [PublicController::class, 'authorsearch'] )->name('authorsearch');

Route::get('/work-with-us', [PublicController::class, 'workWithUs'])->name('workWithUs');

Route::post('/user/send-request', [PublicController::class , 'sendRoleRequest'])->name('sendRoleRequest');

Route::middleware('admin')->group(function(){
    Route::get('/admin/dashboard', [PublicController::class , 'dashboard'])->name('adminDashboard');
    Route::get('/admin/{user}/revisor' , [PublicController::class , 'makeRevisor'])->name('makeRevisor');
    Route::get('/admin/{user}/admin' , [PublicController::class , 'makeAdmin'])->name('makeAdmin');
    Route::get('/admin/{user}/writer' , [PublicController::class , 'makeWriter'])->name('makeWriter');
});