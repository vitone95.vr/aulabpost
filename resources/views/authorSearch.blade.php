<x-layout>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center my-2">Explore Articles by {{$user->name}}</h1>
                
                <div class="row">
                @forelse ($user->articles as $article)
                <div class="col-12 col-md-4 my-2">
                    <div class="card-profile" >
                        <img src="{{Storage::url($article->img)}}"  class="img img-responsive" alt="article image">
                        <div class="card-content">
                            <h5 class="card-title my-2">{{$article->title}}</h5>
                            <div class="d-flex justify-content-between">
                                <a href="{{route('detail', compact('article'))}}" class="btn btn-primary rounded text-center">Details</a>
                              </div>
                        </div>
                    </div>
                </div>
                @empty
                    <div class="col-12">
                        <h1 class="text-center my-2"> There aren't Articles for this Author, try another one</h1>
                    </div>
                @endforelse
                </div>
           
            </div>
        </div>
    </div>

</x-layout>