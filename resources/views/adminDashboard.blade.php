<x-layout>
    <div class="container">
        <div class="row">
            @if (session('admin'))
            <div class="alert alert-success text-center">
                {{ session('admin') }}
            </div>
            @endif
            @if (session('revisor'))
            <div class="alert alert-success text-center">
                {{ session('revisor') }}
            </div>
            @endif
            @if (session('writer'))
            <div class="alert alert-success text-center">
                {{ session('writer') }}
            </div>
            @endif
        </div>
    </div>

    <div class="container my-3">
        <div class="row">
            <div class="col-12">
                <h2>Admin requests</h2>
                <x-admin-requests-table :adminRequests='$adminRequests'/>
            </div>
        </div>
    </div>

    <div class="container my-3">
        <div class="row">
            <div class="col-12">
                <h2>Revisor requests</h2>
                <x-revisor-requests-table :revisorRequests='$revisorRequests'/>
            </div>
        </div>
    </div>

    <div class="container my-3">
        <div class="row">
            <div class="col-12">
                <h2>Writer requests</h2>
                <x-writer-requests-table :writerRequests='$writerRequests'/>
            </div>
        </div>
    </div>
    
</x-layout>