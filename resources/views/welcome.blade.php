<x-layout>

    <div class="container">
        <div class="row">
            @if (session('role'))
            <div class="alert alert-success text-center">
                {{ session('role') }}
            </div>
            @endif    
            @if (session('noadmin'))
            <div class="alert alert-danger text-center">
                {{ session('noadmin') }}
            </div>
            @endif
        <div class="col-12">
            <h2 class="text-center my-3">Most recent Articles</h2>
            <h1 class="text-center my-2">Welcome to Aulab Post</h1>
                <div class="row">
                @foreach ($articles as $article)
                <div class="col-12 col-md-4 my-2">
                    <div class="card-profile" >
                        <img src="{{Storage::url($article->img)}}"  class="img img-responsive" alt="article image">
                        <div class="card-content">
                            <h5 class="card-title my-2">{{$article->title}}</h5>
                            <div class="d-flex justify-content-between">
                                <a href="{{route('detail', compact('article'))}}" class="btn btn-primary rounded text-center">Details</a>
                              </div>
                        </div>
                    </div>
                </div>
                @endforeach
                </div>
           
            </div>
        </div>
    </div>

</x-layout>