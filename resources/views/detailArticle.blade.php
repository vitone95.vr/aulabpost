<x-layout>

    <div class="container">
        <div class="row justify-content-center">

            <h1 class="text-center">
                Here's the article in detail
            </h1>
           
            <div class="col-12 col-md-4 my-2">
                <div class="card-profile" >
                    <img src="{{Storage::url($article->img)}}"  class="img img-responsive" alt="article image">
                    <div class="card-content">
                        <h5 class="card-title my-2">{{$article->title}}</h5>
                        <h5 class="card-title my-2">{{$article->subtitle}}</h5>
                        <h5 class="card-title my-2">{{$article->body}}</h5>
                        <h5 class="card-title my-2">Created {{$article->created_at}}</h5>
                        <a href="{{route('categorysearch', ['category'=>$article->category])}}" class="card-title my-2 btn btn-info">Category :{{$article->category->name}}</a>
                        <a href="{{route('authorsearch', ['user'=>$article->user])}}" class="card-title my-2 btn btn-info">Author :{{$article->user->name}}</a>
                    </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</x-layout>