<div>

    <div class="container">
        <div class="row">
            <h1 class="text-center"> Create article</h1>
            <form wire:submit.prevent='post' enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label class="form-label">Title</label>
                    <input type="text" class="form-control" wire:model="title">
                </div>
                <div class="mb-3">
                    <label class="form-label">Subtitle</label>
                    <input type="text" class="form-control" wire:model="subtitle">
                </div>
                <div class="mb-3">
                    <label for="category" class="form-label">Select a category</label>
                    <select class='form-control' wire:model.defer="category" id='category'>
                        @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>    
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Body</label>
                    <input type="text" class="form-control" wire:model="body">
                </div>
                <div class="mb-3">
                    <label class="form-label">Image</label>
                    <input type="file" class="form-control" wire:model="img">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

</div>
