<x-layout>
    
    @if (session('create'))
    <div class="alert alert-success text-center">
        {{ session('create') }}
    </div>
    @endif
    <livewire:create-article>

</x-layout>