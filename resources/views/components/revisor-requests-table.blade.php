<table class="table">
    <thead>
        <tr>
            <th class="col">#</th>
            <th class="col">Name</th>
            <th class="col">email</th>
            <th class="col">action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($revisorRequests as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <a href="{{route('makeRevisor',$user)}}" class="btn btn-primary">Make Revisor</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>