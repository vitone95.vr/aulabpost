<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
      <a class="navbar-brand" href="{{route('home')}}">Home</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          {{-- <li class="nav-item">
            <a class="nav-link" href="#">Features</a>
          </li> --}}
          @guest          
          <li class="nav-item">
            <a class="nav-link" href="{{route('register')}}">Register</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('login')}}">Login</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link active" href="{{route('create')}}">Create your article</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              Welcome {{ Auth::user()->name }}
            </a>
          </li>
          <li>
            <a class="nav-link" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none" >
            @csrf
            </form>
          </li>
          @if (Auth::user()->is_admin == 1)
          <li class="nav-item">
            <a class="nav-link active" href="{{route('adminDashboard')}}">Admin Dashboard</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link active" href="{{route('workWithUs')}}">Work with us</a>
          </li>
          @endguest

          @endif
        </ul>
      </div>
  </div>
</nav>

<nav class="navbar bg-dark">
  <form class="container-fluid justify-content-evenly">

    <div class="dropdown">
      <a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Search by category
      </a>
      <ul class="dropdown-menu">
        @foreach ($categories as $category)
        <li><a class="dropdown-item" href="{{route('categorysearch', compact('category'))}}">{{$category->name}}</a></li>
        @endforeach
      </ul>
    </div>
    
    <div class="dropdown">
      <a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        Search by author
      </a>
    
      <ul class="dropdown-menu">
        @foreach ($users as $user)
        <li><a class="dropdown-item" href="{{route('authorsearch', compact('user'))}}">{{$user->name}}</a></li>
        @endforeach
      </ul>
    </div>

  </form>
</nav>

