<table class="table">
    <thead>
        <tr>
            <th class="col">#</th>
            <th class="col">Name</th>
            <th class="col">email</th>
            <th class="col">action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($adminRequests as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <a href="{{route('makeAdmin',$user)}}" class="btn btn-primary">Make Admin</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>