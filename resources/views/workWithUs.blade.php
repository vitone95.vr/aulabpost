<x-layout>

    <div class="container my-5">
        <div class="row">
            <div class="col-12 col-md-6">
                <h1>Work with us</h1>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3>Work as Writer</h3>  
                <p>Write articles for us</p>              
                <h3>Work as Revisor</h3>
                <p>Revise articles for us</p>            
                <h3>Work as Admin</h3>
                <p>Administrate the site for us</p>
            </div>
            <div class="col-12 col-md-6">
                <form action="{{route('sendRoleRequest')}}" method="post">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label h3">Wich position are u asking to work?</label>
                        <select class="form-control" name="role" >
                            <option value="admin">Administrator</option>
                            <option value="writer">Writer</option>
                            <option value="revisor">Revisor</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label h5">Insert your email</label>
                        <input type="email" class="form-control" name="email" @auth value="{{ Auth::user()->email }}" @endauth>
                    </div>
                    <div class="mb-3">
                        <label class="form-label h5">Why we should accept you?</label>
                        <textarea name="presentation" class="form-control" id="" cols="30" rows="10"></textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Send</button>
                </form>
            </div>
        </div>
    </div>

</x-layout>